package com.js.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRepairResponse {

    private Long id;

    private String carNumber;

    private String breakDown;

    private LocalDate breakDate;

    private String repairDetails;

    private LocalDate endDate;

    private Double price;

}
