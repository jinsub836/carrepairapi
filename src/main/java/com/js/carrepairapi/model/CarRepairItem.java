package com.js.carrepairapi.model;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class CarRepairItem {

    private Long id;

    private String carNumber;

    private String breakDown;

    private LocalDate breakDate;

    private String repairDetails;

}
