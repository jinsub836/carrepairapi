package com.js.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarRepairStatic {
    private Double totalPrice;
    private Double averagePrice;
}
