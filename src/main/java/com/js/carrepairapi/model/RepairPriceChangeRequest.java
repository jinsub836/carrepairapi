package com.js.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairPriceChangeRequest {
    private Double price;
}
