package com.js.carrepairapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarTax {
    private Double PriceTax;
}
