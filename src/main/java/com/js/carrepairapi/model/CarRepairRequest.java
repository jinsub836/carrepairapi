package com.js.carrepairapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CarRepairRequest {

    private String carNumber;

    private String breakDown;

    private LocalDate breakDate;

    private String repairDetails;

    private LocalDate endDate;

    private Double price;
}
