package com.js.carrepairapi.service;

import com.js.carrepairapi.entity.CarRepair;
import com.js.carrepairapi.model.*;
import com.js.carrepairapi.repository.CarRepairRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarRepairService {
    private final CarRepairRepository carRepairRepository;

    public void setCarRepair(CarRepairRequest request){
        CarRepair addData= new CarRepair();
        addData.setCarNumber(request.getCarNumber());
        addData.setBreakDown(request.getBreakDown());
        addData.setBreakDate(request.getBreakDate());
        addData.setRepairDetails(request.getRepairDetails());
        addData.setEndDate(request.getEndDate());
        addData.setPrice(request.getPrice());

        carRepairRepository.save(addData);
    }

    public List<CarRepairItem> getCarRepair(){
        List<CarRepair> originlist = carRepairRepository.findAll();
        List<CarRepairItem> result = new LinkedList<>();

        for(CarRepair carRepair : originlist){
          CarRepairItem addItem = new CarRepairItem();
          addItem.setId(carRepair.getId());
          addItem.setCarNumber(carRepair.getCarNumber());
          addItem.setBreakDown(carRepair.getBreakDown());
          addItem.setBreakDate(carRepair.getBreakDate());
          addItem.setRepairDetails(carRepair.getRepairDetails());

          result.add(addItem);
        } return result;
    }

    public CarRepairResponse getRepair(Long id){
        CarRepair originData = carRepairRepository.findById(id).orElseThrow();
        CarRepairResponse response = new CarRepairResponse();

        response.setId(originData.getId());
        response.setCarNumber(originData.getCarNumber());
        response.setBreakDown(originData.getBreakDown());
        response.setBreakDate(originData.getBreakDate());
        response.setRepairDetails(originData.getRepairDetails());
        response.setEndDate(originData.getEndDate());
        response.setPrice(originData.getPrice());

        return response;
    }
    public void  putPrice(long id , RepairPriceChangeRequest request ){
        CarRepair originData  = carRepairRepository.findById(id).orElseThrow();

        originData.setPrice(request.getPrice());

        carRepairRepository.save(originData);
    }

    public List<CarTax> getTax(){
        List<CarRepair> originData = carRepairRepository.findAll();
        List<CarTax> result = new LinkedList<>();


        for(CarRepair carRepair : originData){
            CarTax taxPrice= new CarTax();

          taxPrice.setPriceTax(carRepair.getPrice()*1.033);

            result.add(taxPrice);
        }
    return result;}

    public CarRepairStatic getStatic(){
        CarRepairStatic carRepairStatic = new CarRepairStatic();
        List<CarRepair> originlist = carRepairRepository.findAll();

        double totalPrice= 0D;

        for(CarRepair carRepair : originlist){
            totalPrice += carRepair.getPrice();
        }
        double averagePrice = totalPrice/originlist.size();

        carRepairStatic.setTotalPrice(totalPrice);
        carRepairStatic.setAveragePrice(averagePrice);

        return carRepairStatic;
    }

}

