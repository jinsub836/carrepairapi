package com.js.carrepairapi.repository;

import com.js.carrepairapi.entity.CarRepair;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepairRepository extends JpaRepository<CarRepair, Long> {
}
