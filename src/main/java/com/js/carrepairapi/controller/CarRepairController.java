package com.js.carrepairapi.controller;

import com.js.carrepairapi.model.*;
import com.js.carrepairapi.service.CarRepairService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carRepair")
public class CarRepairController {
    private final CarRepairService carRepairService;

    @PostMapping("/new")
    public String setCarRepair(@RequestBody CarRepairRequest request){
        carRepairService.setCarRepair(request);
        return "등록 완료";
    }

    @GetMapping("/all")
    public List<CarRepairItem> getCarRepair(){
    return carRepairService.getCarRepair();
    }

    @GetMapping("/detail/{id}")
    public CarRepairResponse getRepair(@PathVariable long id){
        return carRepairService.getRepair(id);
    }

    @GetMapping("/static")
    public CarRepairStatic getStatic(){
        return carRepairService.getStatic();
    }

    @GetMapping("/tax")
    public List<CarTax> getTax() { return carRepairService.getTax();}

    @PutMapping("/price/{id}")
    public String putPrice(@PathVariable long id, @RequestBody RepairPriceChangeRequest request){
        carRepairService.putPrice(id,request);
        return "수정 완료";
    }

}
